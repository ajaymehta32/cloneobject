package ajaymehta.cloneobject;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {


    // 1 )if i talk about try and catch...u have to just use at one place...either where you are calling a method (where exception occour )  or you can define at the method.that is being called..
    // 2) other way to use throws...you have to use this keyword at both the places ..from where you are calling the method (where exception occour ) and at that method that is being called
    //   2) is implemented..you can see below..
    //  1) is commented...you can also check that...




    Student student1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


    }

    public void studentData(View view) {  // real and clone object are in the same class so not much of use..

        student1 = new Student("Ajay", 32);
        student1.print(this);

    }

    public void duplicateStudentData(View view) throws CloneNotSupportedException {   // u can also use try catch here insted of throws keyword..

        Student studentCopy = (Student) student1.cloneObject();
        studentCopy.print(this);
    }
}


// yes there can be interfaces without any method called marking interface...clonable and serilizable are marking interface.
// Colneable interface is use to make clone of a class object

class Student implements Cloneable {

    private String name;
    private int rollno;


    public Student(String name, int rollno) {
        this.name = name;
        this.rollno = rollno;
    }

    // cloning object with the help of clone() method..


// try catch example instead of throws keyword...

/*    protected Object cloneObject() {   // if you want an object to get cloned from this package only use protected..

        try {
            return super.clone();
        } catch (CloneNotSupportedException e) {
            e.printStackTrace();
        }

        return null;
    }*/


    // see here is the thing....if you dont wanna use try catch block...there is alternative way...to is to use   throws keyword..to throw exception.

    protected Object cloneObject() throws CloneNotSupportedException {

        return super.clone();
    }


    public void print(Context context) {

        Toast.makeText(context, "Name and Roll no of student is " + name + " " + rollno, Toast.LENGTH_SHORT).show();
    }
}
